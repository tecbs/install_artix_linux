#!/bin/bash
DISK=""
BOOT_SIZE="512" # MEGA
RAM_SIZE="1"    # GIGA
ROOT_SIZE="100%"

# Calcular el tamaño final de la partición de swap en megabytes
RAM_SIZE_END=$((BOOT_SIZE + RAM_SIZE * 1024))

select_disk() {
    available_disks=($(lsblk --noheadings --raw -d | awk '{print $1"("$4")"}'))

    if [ ${#available_disks[@]} -eq 0 ]; then
        echo "No se encontraron discos disponibles."
        exit 1
    fi

    available_disks+=("Salir")

    PS3="Por favor, elige un disco de la lista (o 'Salir' para salir): "
    select selected_disk in "${available_disks[@]}"; do
        if [ "$selected_disk" = "Salir" ]; then
            echo "Saliendo del programa."
            exit 0
        elif [ -n "$selected_disk" ]; then
            DISK=$(echo "$selected_disk" | sed 's/([^)]*)//')
            echo "Has seleccionado el disco: $DISK"
            break
        else
            echo "Selección no válida. Por favor, elige un número de la lista o 'Salir' para salir."
        fi
    done
}

create_partitions() {
    # Crear particiones con parted
    parted "/dev/${DISK}" mklabel gpt
    parted "/dev/${DISK}" mkpart primary ext4 1M ${BOOT_SIZE}M
    parted "/dev/${DISK}" mkpart primary linux-swap ${BOOT_SIZE}M ${RAM_SIZE_END}M
    parted "/dev/${DISK}" mkpart primary ext4 ${RAM_SIZE_END}M ${ROOT_SIZE}
    parted "/dev/${DISK}" set 1 boot on
}

format_partitions() {
    # Formatear particiones
    mkfs.fat -F32 "/dev/${DISK}1"
    fatlabel /dev/${DISK}1 ESP
    mkswap -L SWAP "/dev/${DISK}2"
    mkfs.ext4 -L ROOT "/dev/${DISK}3"
}

mount_partitions() {
    # Montar particiones
    swapon /dev/disk/by-label/SWAP
    mount /dev/disk/by-label/ROOT /mnt
    mkdir -p /mnt/boot/efi
    mount /dev/disk/by-label/ESP /mnt/boot/efi
}

# Ejecutar funciones
select_disk
create_partitions
format_partitions
mount_partitions
