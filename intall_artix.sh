#!/bin/bash
#########################################################################################
DISK=""
BOOT_SIZE="512" #MEGA
RAM_SIZE="1" #GIGA
ROOT_SIZE="100%"
USER_I="artix"
HOSTNAME_I="artix-vitual"
PASSWD_USER="123"
PASSWD_ROOT="123"
ZONE="Europe/Madrid"
LANG_I="es_ES.UTF-8"
LANG_T="UTF-8"
KEYMAP_I="es"
PORTSSH="3322"
#
RAM_SIZE_END=$((BOOT_SIZE+RAM_SIZE*1024))
#
INSTALLOTHERS="neovim curl wget neofetch xorg xorg-xinit libxft xf86-video-fbdev"
#
directfile="https://gitlab.com/tecbs/install_dwm_arch/-/raw/main/"
archivocsv="programas.csv"
srcdir="/home/$USER_I/.local/src"
bindir="/home/$USER_I/.local/bin"
configdir="/home/$USER_I/.config"
installdir="/home/$USER_I/instalador"
progsfile="$installdir/$archivocsv"
version="dwm-6.4-tecbs"

###########################################################################################

delete_umount() {
    # Comprobar si la partición swap está montada
    if grep -q "/dev/${DISK}2" /proc/swaps; then
        echo "Desmontando swap..."
        swapoff /dev/${DISK}2
    else
        echo "La partición swap no está montada."
    fi

    # Comprobar si /mnt está montado
    if mountpoint -q /mnt; then
        echo "Desmontando /mnt..."
        umount -R /mnt
    else
        echo "/mnt no está montado."
    fi

    # Comprobar si existe el directorio /mnt
    if [ -d "/mnt" ]; then
        echo "Borrando /mnt..."
        rm -rf /mnt
    else
        echo "/mnt no existe."
    fi

    # Reiniciar la partición con sgdisk solo si existe
    sgdisk --zap-all "/dev/${DISK}"
    sleep 2s
}

current_function_name() {
    # Imprime el nombre de la función actual
    echo "${FUNCNAME[1]}"
}

select_disk() {
    available_disks=($(lsblk --noheadings --raw -d | awk '{print $1"("$4")"}'))

    if [ ${#available_disks[@]} -eq 0 ]; then
        echo "No se encontraron discos disponibles."
        exit 1
    fi

    available_disks+=("Salir")

    PS3="Por favor, elige un disco de la lista (o 'Salir' para salir): "
    select selected_disk in "${available_disks[@]}"; do
        if [ "$selected_disk" = "Salir" ]; then
            echo "Saliendo del programa."
            exit 0
        elif [ -n "$selected_disk" ]; then
            DISK=$(echo "$selected_disk" | sed 's/([^)]*)//')
            echo "Has seleccionado el disco: $DISK"
            break
        else
            echo "Selección no válida. Por favor, elige un número de la lista o 'Salir' para salir."
        fi
    done
}

create_partitions() {
    echo "Ejecutando función: $(current_function_name)"
    parted "/dev/${DISK}" mklabel gpt
    parted "/dev/${DISK}" mkpart primary ext4 1Mib ${BOOT_SIZE}Mib
    parted "/dev/${DISK}" mkpart primary linux-swap ${BOOT_SIZE}Mib ${RAM_SIZE_END}Mib
    parted "/dev/${DISK}" mkpart primary ext4 ${RAM_SIZE_END}Mib ${ROOT_SIZE}
    parted "/dev/${DISK}" set 1 boot on
}
 
format_partitions(){
    echo "Ejecutando función: $(current_function_name)"
    mkfs.fat -F32 "/dev/${DISK}1"
    fatlabel /dev/${DISK}1 ESP
    mkswap -L SWAP /dev/${DISK}2
    mkfs.ext4 -L ROOT /dev/${DISK}3

}

mount_partitions(){
    echo "Ejecutando función: $(current_function_name)"
    swapon /dev/disk/by-label/SWAP
    mkdir /mnt
    mount /dev/disk/by-label/ROOT /mnt
    mkdir /mnt/boot
    mkdir /mnt/boot/efi
    mount /dev/disk/by-label/ESP /mnt/boot/efi
}


update_time(){
    echo "Ejecutando función: $(current_function_name)"
    rc-service ntpd start
}

install_base(){
    echo "Ejecutando función: $(current_function_name)"
    # Instalar el sistema base
    basestrap /mnt base base-devel openrc elogind-openrc git
}

install_kernel(){
    echo "Ejecutando función: $(current_function_name)"
    basestrap /mnt linux linux-firmware sudo
}

create_fstab(){
    echo "Ejecutando función: $(current_function_name)"
    # Generar archivo fstab
    fstabgen -U /mnt >> /mnt/etc/fstab
}


chroot_zone(){
    echo "Ejecutando función: $(current_function_name)"
    artix-chroot /mnt /bin/bash <<-EOF
        ln -sf /usr/share/zoneinfo/${ZONE} /etc/localtime 
EOF
}

chroot_clock(){
    echo "Ejecutando función: $(current_function_name)"
    artix-chroot /mnt /bin/bash <<-EOF
        hwclock --systohc
EOF
}

chroot_lang(){
    echo "Ejecutando función: $(current_function_name)"
    artix-chroot /mnt /bin/bash <<-EOF
        sed -i 's/#${LANG_I} ${LANG_T}/${LANG_I} ${LANG_T}/' /etc/locale.gen
        locale-gen
        echo 'export LANG="'"${LANG_I}"'"' > /etc/locale.conf
        echo 'export LC_COLLATE="'"C"'"' >> /etc/locale.conf
EOF
}

chroot_keymaps(){
    echo "Ejecutando función: $(current_function_name)"
    artix-chroot /mnt /bin/bash <<-EOF
        sed -i 's/^keymap.*/keymap="'"${KEYMAP_I}"'"/' /etc/conf.d/keymaps
EOF
}

chroot_grub(){
    echo "Ejecutando función: $(current_function_name)"
    artix-chroot /mnt /bin/bash <<-EOF
        pacman -S --noconfirm grub efibootmgr
        grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB
        grub-mkconfig -o /boot/grub/grub.cfg
EOF
}

chroot_hostname (){
    echo "Ejecutando función: $(current_function_name)"
    artix-chroot /mnt /bin/bash <<-EOF
        echo ${HOSTNAME_I} > /etc/hostname
EOF
}

chroot_hosts(){
    echo "Ejecutando función: $(current_function_name)"
    artix-chroot /mnt /bin/bash <<-EOF
        echo "127.0.0.1 localhost" >> /etc/hosts
        echo "::1       localhost" >> /etc/hosts
        echo "127.0.1.1 ${HOSTNAME_I}.localdomain ${HOSTNAME_I}" >> /etc/hosts
EOF
}

chroot_connmand (){
    echo "Ejecutando función: $(current_function_name)"
    artix-chroot /mnt /bin/bash <<-EOF
        pacman -S --noconfirm dhcpcd connman-openrc
        rc-update add connmand
EOF
}

chroot_sudo (){
    echo "Ejecutando función: $(current_function_name)"
    artix-chroot /mnt /bin/bash <<-EOF
        echo "${USER_I} ALL=(ALL) ALL" > /etc/sudoers.d/00_${USER_I}
EOF
}

chroot_sshd (){
    echo "Ejecutando función: $(current_function_name)"
    artix-chroot /mnt /bin/bash <<-EOF
        pacman -S --noconfirm openssh-openrc
        sed -i 's/^#Port\s.*/Port ${PORTSSH}/' /etc/ssh/sshd_config
        rc-update add sshd default
        #rc-service sshd start
        sleep 4s
EOF
}

chroot_adduser(){
    echo "Ejecutando función: $(current_function_name)"
    artix-chroot /mnt /bin/bash <<-EOF
        useradd -m ${USER_I}
EOF
}

chroot_pacmanconfig(){
    echo "Ejecutando función: $(current_function_name)"
    artix-chroot /mnt /bin/bash <<-EOF
        sed -i 's/^#Color/Color/' /etc/pacman.conf
        sed -i 's/^#ParallelDownloads.*/ParallelDownloads = 5/' /etc/pacman.conf      
EOF
}

chroot_autologin(){
    echo "Ejecutando función: $(current_function_name)"
    artix-chroot /mnt /bin/bash <<-EOF
        sed -i "s/^agetty_options.*/agetty_options=\"--autologin ${USER_I} --noclear tty1 linux\"/" /etc/conf.d/agetty.tty1
EOF
}

create_dirs(){
    echo "Ejecutando función: $(current_function_name)"
    artix-chroot /mnt /bin/bash <<-EOF
    [ -d "$srcdir" ] && echo "$srcdir existe" || mkdir -p "$srcdir" || echo "$srcdir creado"
    [ -d "$bindir" ] && echo "$bindir existe" || mkdir -p "$bindir" || echo "$bindir creado"
    [ -d "$configdir" ] & echo "$configdir existe" || mkdir -p "$configdir" || echo "$configdir creado"
    [ -d "$installdir" ] && rm -rf "$installdir" && echo "$installdir borrado"
    [ -d "$installdir" ] || mkdir -p "$installdir" && echo "$installdir creado"
EOF
}


chroot_password(){
    echo "Ejecutando función: $(current_function_name)"
    artix-chroot /mnt /bin/bash <<-EOF
        echo -e "$PASSWD_USER\n$PASSWD_USER" | passwd ${USER_I}
        echo -e "$PASSWD_ROOT\n$PASSWD_ROOT" | passwd
EOF
}


chroot_installothers(){
    echo "Ejecutando función: $(current_function_name)"
    artix-chroot /mnt /bin/bash <<-EOF
        pacman -S --noconfirm ${INSTALLOTHERS}
EOF
}


select_disk
delete_umount
create_partitions
format_partitions
mount_partitions
update_time
install_base
install_kernel
create_fstab
chroot_zone
chroot_clock
chroot_lang
chroot_keymaps
chroot_grub
chroot_hostname
chroot_hosts
chroot_connmand
chroot_sudo
chroot_sshd
chroot_adduser
chroot_pacmanconfig
chroot_autologin
create_dirs
chroot_password
chroot_installothers



echo "El valor seleccionado es $DISK"

# Desmontar particiones
umount -R /mnt
swapoff /dev/${DISK}2

echo "La instalación ha finalizado. Puedes reiniciar tu sistema."
